"use strict";
const electron = require("electron");
// const app = electron.app;
const app = electron.app || electron.remote.app;

var os = require("os");
const dialog = electron.dialog || electron.remote.dialog;
// const {dialog} = require("electron");

const BrowserWindow = electron.BrowserWindow;

const { ipcMain } = require("electron");

const path = require("path");
const isDev = require("electron-is-dev");
const fs = require("fs");
const remote = require("electron").remote;
app.allowRendererProcessReuse = false;

const fse = require("fs-extra");

var desktop = path.join("C:/Users", "s.alrajabi/Desktop");
var z = path.join(`${__dirname}`);
console.log("z", z);
const filePath = path.join(__dirname, "/ppppp.txt");
console.log(filePath);
console.log("desktop", desktop);
var s = app.getAppPath("./Folder");
console.log("s", s);

const {
  CATCH_ON_MAIN_ELECTRON,
  SEND_TO_RENDER,
  CREATE_ON_THE_ROOT,
  CREATE_ON_PUBLIC,
  CREATE_ON_FOLDER,
  CREATE_ON_DESKTOP,
  READ_FOLDER,
  FILE,
} = require("./constant.js");
const testFolder = "C:/test/";

let mainWindow;
function createWindow() {
  mainWindow = new BrowserWindow({
    width: 900,
    height: 680,
    webPreferences: {
      contextIsolation: false,

      enableremotemodule: true,

      nodeIntegration: true,
      preload: __dirname + "/preload.js",
    },
  });

  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );
  mainWindow.on("closed", () => (mainWindow = null));

  // retrieve event that is send by renderer process

  ipcMain.on("open-file-diolog-for-file", (event) => {
    //checking the operation system for the user

    if (os.platform() === "linux" || os.platform() === "win32") {
      dialog
        .showOpenDialog({ properties: ["openFile"] })
        .then((result) => {
          console.log("result", result);
          if (result.canceled === false) {
            event.sender.send("selected-file", result.filePaths[0]);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      // this is  mac
      dialog.showOpenDialog(
        {
          properties: ["openFile", "openDirectory"],
        },
        function (files) {
          if (files) {
            event.sender.send("selected-file", files[0]);
          }
        }
      );
    }
  });
  // Open the DevTools.
  mainWindow.webContents.openDevTools();
}

//handeling send data from renderer(react) to main (electron.js)
ipcMain.on(CATCH_ON_MAIN_ELECTRON, (event, arg) => {
  console.log("sending data renderer(react) to main (electron.js)", arg);
  mainWindow.send(SEND_TO_RENDER, arg);
});



//CREATE_ON_PUBLIC
ipcMain.on(CREATE_ON_PUBLIC, (event, arg) => {
  console.log("from (react) to  (electron.js)", arg);
  // fs writeFile (path for my new file , console log electron , content on the file )
  fs.writeFile(
    `${__dirname}/${arg}`,
    "console.log('done in the public')----> FILE NAME",
    function (err) {
      console.log("create done in the public");
    }
  );
});

//CREATE_ON_ROOT
ipcMain.on(CREATE_ON_THE_ROOT, (event, arg) => {
  console.log("from (react) to  (electron.js)----> FILE NAME", arg);
  console.log(event.reply);
  // fs writeFile (path for my new file , console log electron , content on the file )
  fs.writeFile(`./${arg}`, "console.log('done in the ROOT')", function (err) {
    console.log("create done in the ROOT");
  });
});

////////////////////////////////////Task//////////////////////////////////////
const options = {
  type: "question",
  buttons: ["Cancel"],
  defaultId: 2,
  title: "pop up",
  message: "this file is exists ",
};

const options2 = {
  type: "question",
  buttons: ["Cancel"],
  defaultId: 2,
  title: "pop up",
  message: "you have  successfully create new  file ",
};
const options3 = {
  type: "question",
  buttons: ["Cancel"],
  defaultId: 2,
  title: "pop up",
  message: "you have  successfully  delete all files ",
};


//////////////////////////////////////////////////////////////////////////


//handeling send data from renderer(react) to main (electron.js)



ipcMain.on("FILE", (event, arg) => {
  mainWindow.send(SEND_TO_RENDER, arg);
  const pathNew = path.join("C:/test", arg.name);

  if (fs.existsSync(pathNew)) {
    // path exists
    console.log("exists:", pathNew);
    dialog.showMessageBox(null, options, (response) => {
      console.log(response);
    });
  } else {
    console.log("DOES NOT exist:", pathNew);
    fs.writeFile(
      pathNew,
      "console.log('create new folder')",
      function (err) {
        console.log("create new folder in test ");
        dialog.showMessageBox(null, options2, (response) => {
          console.log(response);
          fs.readdir(testFolder, (err, files) => {
            files.forEach((file) => {
              event.reply("send file", files);
            });
          });
        });
      }
    );
   
    
  }
  /// table or list to show all files in that destination

 
  fs.readdir(testFolder, (err, files) => {
    files.forEach((file) => {
      event.reply("send file", files);
    });
  });
});



//////////////////////////////////////////////////////////////////////////
//delete all file

ipcMain.on("delete-all-file", (event, arg) => {
  console.log("delete all ");
  const testFolder = "C:/test/";
  fs.readdir(testFolder, (err, files) => {
    if (err) throw err;
   
    for (const file of files) {
//       fs.unlinkSync(path.join(testFolder, file));
// console.log('File deleted!');
   
      fs.unlink(path.join(testFolder, file), (err) => {
        if (err) {
          console.log("failed to delete local image:"+err);
      } else {
        event.reply("send file", files);
          console.log('successfully deleted local image');                                
      }
    
      }
      
    );
    console.log("in folderrrr" ,files )
   
    // fs.readdir(testFolder, (err, files) => {
    //   files.forEach((file) => {
    //     console.log("dddd", files)
    //     event.reply("send file", files);
    //   });
    // });
 
      
      
    }
    dialog.showMessageBox(null, options3, (response) => {
      console.log(response);
    });
  });
});

app.on("ready", createWindow);
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});
app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});
