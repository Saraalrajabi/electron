import React, { Component } from "react";
import "./App.css"
const { ipcRenderer } = window.require("electron");
const electron = window.require("electron");

const {
  CREATE_ON_THE_ROOT,
  CREATE_ON_PUBLIC,
  CREATE_ON_FOLDER,
  CREATE_ON_DESKTOP,
  READ_FOLDER,
  CATCH_ON_MAIN_ELECTRON,
  SEND_TO_RENDER,
  FILE,
} = require("./constant.js");

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      res: [],
    
    };
  }

  componentDidMount() {
    // ipcRenderer.on(SEND_TO_RENDER, this.handleRenderer);
    ipcRenderer.on("send file", this.handleRenderer);
  }
  componentWillUnmount() {
    ipcRenderer.removeListener(SEND_TO_RENDER, this.handleRenderer);
  }
  handleRenderer = (event, data) => {
 
    this.setState({
      res: data,
    });

  
  };
  handelFile = (event) => {
   
     var obj = event.target.files[0];
    let  arg = { name: obj.name, path: obj.path  , type :obj.type};   
    ipcRenderer.send("FILE" , arg);

  };

  handleOnClick = () => {
    console.log("send data from react to electron");
    ipcRenderer.send(CATCH_ON_MAIN_ELECTRON, " dataaaaa from react  ");
  };

  handleOnClickRoot = () => {
    console.log("clicked Root");
    ipcRenderer.send(CREATE_ON_THE_ROOT, " rootNewFile.txt");
  };
  handleOnClickPublic = () => {
    console.log("click public");
    //ipcRenderer.send with create file (title , name new file)
    ipcRenderer.send(CREATE_ON_PUBLIC, " InPublicNewFile.txt");
  };

  handleOnClickDesktop = () => {
    console.log("click desktop");
    ipcRenderer.send(CREATE_ON_DESKTOP, "desktop.txt");
  };
  handleDelete = () => {
    ipcRenderer.send("delete-all-file", this.state.res);

  };

  render() {
  
    return (
      <div className="App">
        <div>
          <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <td>Files List</td>
            </tr>
            {this.state.res.map((value, index) => {
              return (
                <tr key={index}>
                  <td>{value}</td>
                </tr>
              );
            })}
            </tbody>
          </table>
        </div>
       
        {/* <button onClick={this.handleOnClick}>
          send data from react to electron{" "}
        </button>
        <br />
        <button onClick={this.handleOnClickPublic}>
          create new file on other directory(public)
        </button>
        <br />
        <button onClick={this.handleOnClickRoot}>
          create new file in the root
        </button> */}
        <br />
        <br />
        {/* <button onClick={this.handleOnClickDesktop}>create  file</button> */}
        
       
        <h3>File Upload in Electron</h3>
        {/* <button id="upload">Upload File</button> */}
        <br />
        <input id="uploadFile" type="file" onChange={this.handelFile} />
        <br />
        <br />
        <button onClick={this.handleDelete}>delete all file</button>
      </div>
    );
  }
}

export default App;
